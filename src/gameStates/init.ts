import gameLogic from "../gameElements/gameLogic";
import gamePlay from "../gamePlay";

export default function init(scene : any) {
    gameLogic.init(scene);
    gamePlay.nextState();
};