import gameLogic from "../gameElements/gameLogic";
import display from "../gameElements/display";

export default function endGame() {
    display.list.replayButton.visible = true;
    display.list.replayButton.enable();
}