import gameLogic from "../gameElements/gameLogic";
import gamePlay from "../gamePlay";

export default function reset() {
    gameLogic.reset();
    gamePlay.nextState();
};