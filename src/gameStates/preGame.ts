import gameLogic from "../gameElements/gameLogic";

export default function init() {
    gameLogic.preGame();
};