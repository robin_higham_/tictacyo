import gameLogic from "../gameElements/gameLogic";
import {game} from "../index";

export default function enable() {
    gameLogic.enable();
    gameLogic.selectFirstPlayer();
};