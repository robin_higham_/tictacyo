enum stateMachine {
    init,
    preGame,
    enable,
    endGame,
    reset
}

export default class StateMachine {
    currentState: stateMachine;
    init: any;
    enable: any;
    reset: any;
    endGame: any;
    preGame: any;

    constructor() {
        this.currentState = stateMachine.init;
    }

    public setStates(props: any) {
        this.init = props.init;
        this.preGame = props.preGame;
        this.enable = props.enable;
        this.endGame = props.endGame;
        this.reset = props.reset;
    }

    public next() {
        switch (this.currentState) {
            case stateMachine.init: {
                this.currentState = stateMachine.preGame;
                this.preGame();
                break;
            }
            case stateMachine.preGame: {
                this.currentState = stateMachine.enable;
                this.enable();
                break;
            }
            case stateMachine.enable: {
                this.currentState = stateMachine.endGame;
                this.endGame();
                break;
            }
            case stateMachine.endGame: {
                this.currentState = stateMachine.reset;
                this.reset();
                break;
            }
            case stateMachine.reset: {
                this.currentState = stateMachine.enable;
                this.enable();
                break;
            }
        }
        console.log("CURRENT STATE: "+ this.currentState);
    }
}
