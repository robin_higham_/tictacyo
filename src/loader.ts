import manifest from './manifest.json';

function init() {

}

function load(scene : any) {

    scene.load.setBaseURL("/assets/");

    let data : any;
    data = manifest;

    Object.keys(data).forEach(e=>{
        scene.load.image(e,data[e]);
        console.log(e);
    });

    scene.load.audio('music', '/audio/music.mp3');
    scene.load.audio('player', '/audio/player.mp3');
    scene.load.audio('computer', '/audio/computer.mp3');
    scene.load.audio('win', '/audio/win.mp3');
    scene.load.audio('lose', '/audio/lose.mp3');
}

export default {
    load,
    init
}