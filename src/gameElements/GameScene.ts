import * as Phaser from 'phaser';
import gamePlay from "../gamePlay";
import loader from "../loader";
import {game} from "../index";

let config: Phaser.Types.Scenes.SettingsConfig = {
    key: 'gamePlay',
    visible: true,
    active: true
};

export default class gameScene extends Phaser.Scene {

    constructor() {
        super(config);
    }

    public preload() {
        loader.load(this);
    }

    public create() {
        gamePlay.start(this);
    }
}