import * as Phaser from 'phaser';
import {game} from '../index';


export default class Point extends Phaser.GameObjects.Sprite {
    value : string;
    index : number;
    revealed : boolean;
    resolve : any;
    constructor(props : any, index : number) {
        super(props.scene,props.x,props.y, "space" );
        this.value = "";
        this.index = index;
        this.revealed = false;
        this.alpha = 0.01;

        props.scene.add.existing(this);

        this.on('pointerdown',()=>{
            this.uncover();
        });

        this.on('pointerover',()=>{
            this.alpha = 1;
        });
        this.on('pointerout',()=>{
            if(!this.revealed) {
                this.alpha = 0.01;
            }
        });

        console.log(this);
    }

    enable() {
        return new Promise(resolve =>{
            this.resolve = resolve;
        });
    }

    uncover(){
        this.resolve();
    }

    unlock() {
        this.setInteractive();
    }

    lock() {
        this.disableInteractive();
    }

    assign(identifier : string) {
        return new Promise(resolve => {
            this.value = identifier === 'X' ? "X" : "O";
            this.revealed = true;
            this.setTexture(identifier === 'X' ? "cross" : "nought");

            this.scaleX = 5;
            this.scaleY = 5;

            this.scene.tweens.add({
                targets:[this],
                alpha:1,
                scale: 1,
                duration:100,
                onComplete : resolve
            });
        });
    }

    setWin() {
        this.setTexture(this.value === "X" ? "cross_win" : "nought_win");
    }

    reset() {
        this.value = "";
        this.revealed = false;
        this.setTexture("space");
        this.alpha = 0.01;

        this.scaleX = 1;
        this.scaleX = 1;
    }

}