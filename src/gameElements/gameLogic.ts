import Point from "./Point";
import gamePlay from "../gamePlay";
import display from "./display";
import {game} from "../index";

enum Players {
    pc,
    computer
}

let possibleWins = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [1, 5, 9],
    [3, 5, 7],
    [1, 4, 7],
    [3, 6, 9],
    [2, 5, 8]
];

let selected: number;
selected = 0;

let pointArray: any[];
let player: Players;

let playerSelector: string;
let computerSelector: string;

let currentSelector: string;
let winningLine: boolean;

let playerScore = 0;
let computerScore = 0;

let gameScene: any;

function init(scene: any) {

    gameScene = scene;

    pointArray = [
        new Point({scene: gameScene, x: 476.2, y: 352.2, texture: ""}, 1),
        new Point({scene: gameScene, x: 706.5, y: 352.2, texture: ""}, 2),
        new Point({scene: gameScene, x: 936.8, y: 352.2, texture: ""}, 3),
        new Point({scene: gameScene, x: 476.2, y: 581.5, texture: ""}, 4),
        new Point({scene: gameScene, x: 706.5, y: 581.5, texture: ""}, 5),
        new Point({scene: gameScene, x: 936.8, y: 581.5, texture: ""}, 6),
        new Point({scene: gameScene, x: 476.2, y: 810.8, texture: ""}, 7),
        new Point({scene: gameScene, x: 706.5, y: 810.8, texture: ""}, 8),
        new Point({scene: gameScene, x: 936.8, y: 810.8, texture: ""}, 9),
    ];

    display.generateDisplayList(gameScene);

    display.list.playButton.on('pointerup', () => {
        gamePlay.nextState();
        display.list.playButton.disabled();
        display.list.playButton.visible = false;
    });

    display.list.replayButton.on('pointerup', () => {
        gamePlay.nextState();
        display.list.replayButton.disabled();
        display.list.replayButton.visible = false;
    });

    display.list.replayButton.visible = false;

    display.list.background.depth = -1;

    game.sound.add("music");
    game.sound.add("player");
    game.sound.add("computer");
    game.sound.add("win");
    game.sound.add("lose");

    game.sound.play("music", {
        loop: true
    });
}

function preGame() {
    display.list.playButton.enable();
    display.list.replayButton.disabled();
}

function selectFirstPlayer() {
    let decider = Math.round(Math.random());

    if (decider === 1) {
        player = Players.pc;

        playerSelector = "O";
        computerSelector = "X";
    } else {
        player = Players.computer;

        playerSelector = "X";
        computerSelector = "O";
    }
    nextTurn();
}

function nextTurn() {
    if (player === Players.pc) {
        player = Players.computer;

        currentSelector = computerSelector;

        pointArray.forEach(e => {
            e.lock();
        });

        game.sound.play("player");
        computerTurn();
    } else {
        player = Players.pc;

        currentSelector = playerSelector;

        pointArray.forEach(e => {
            if (!e.revealed) {
                e.unlock();
            }
        })
    }
}

function computerTurn() {
    gameScene.time.addEvent({
        delay: 500,
        callback: () => {
            let unused: any[] = [];
            let assigned: boolean = false;

            possibleWins.forEach(e => {
                if (!assigned) {
                    let playerValues: any[] = [];
                    e.forEach(el => {
                        if (pointArray[el - 1].value !== "") {
                            if(pointArray[el - 1].value === playerSelector) {
                                playerValues.push(pointArray[el - 1]);
                            }
                        }
                    });
                    if (playerValues.length === 2) {
                        e.forEach(el => {
                            if (pointArray[el - 1].value === "") {
                                assigned = true;
                                pointArray[el - 1].uncover();
                            }
                        });
                    }
                }
            });

            if(!assigned) {
                pointArray.forEach(e => {
                    if (e.revealed === false) {
                        unused.push(e);
                    }
                });

                let random = Math.floor(Math.random() * unused.length);

                unused[random].uncover();
            }

            game.sound.play("computer");
        },
    });
}

function enable() {
    pointArray.map(async e => {
        await e.enable();
        await e.assign(currentSelector);
        await checkLine();
        if (!winningLine) {
            selected++;
            if (selected === 9) {
                winningLine = true;
                gameScene.time.addEvent({
                    delay: 2000,
                    callback: gamePlay.nextState
                });
            } else {
                nextTurn();
            }
        }
    });
}

function checkLine() {
    return new Promise<void>(resolve => {
        possibleWins.forEach(element => {
            if (element.every(el => {
                return pointArray[el - 1].value === currentSelector;
            })) {
                element.forEach(e => {
                    pointArray[e - 1].setWin();
                });
                pointArray.forEach(e => {
                    e.lock();
                });
                winningLine = true;
                updateScore();
                gameScene.time.addEvent({
                    delay: 2000,
                    callback: gamePlay.nextState
                });
            } else {
                resolve();
            }
        });
    });
}

function updateScore() {
    if (player === Players.pc) {
        playerScore++;
        display.list.playerWinsValue.text = playerScore;
        game.sound.play("win");
    } else {
        computerScore++;
        display.list.computerWinsValue.text = computerScore;
        game.sound.play("lose");
    }

}

function reset() {
    pointArray.forEach(e => {
        e.reset();
    });
    winningLine = false;

    playerSelector = "";
    computerSelector = "";
    currentSelector = "";

    selected = 0;
}

export default {
    init,
    preGame,
    enable,
    reset,
    selectFirstPlayer
}