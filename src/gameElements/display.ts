import textStyles from "../display/textStyles";
import displayList from "../display/displayList";
import Button from "../display/Button";

let list : any;
list = {};

function generateDisplayList(scene : any) {
    let tempDisplayList : any;
    tempDisplayList = displayList;

    Object.keys(tempDisplayList).forEach(e=>{
        switch (tempDisplayList[e].type) {
            case "sprite": {
                list[e] = new Phaser.GameObjects.Sprite(
                    scene,
                    tempDisplayList[e].x,
                    tempDisplayList[e].y,
                    tempDisplayList[e].texture
                );
                break;
            }
            case "button" : {
                list[e] = new Button(
                    {
                        scene:scene,
                        x:tempDisplayList[e].x,
                        y:tempDisplayList[e].y,
                        texture:""
                    } , {
                        enabled:tempDisplayList[e].states.enabled,
                        pressed:tempDisplayList[e].states.pressed,
                        over:tempDisplayList[e].states.over,
                    }
                );
                break;
            }
            case "text" : {
                list[e] = new Phaser.GameObjects.Text(
                    scene,
                    tempDisplayList[e].x,
                    tempDisplayList[e].y,
                    tempDisplayList[e].string,
                    generateTypeStyles(tempDisplayList[e].style)
                )
            }
        }
        scene.add.existing(list[e]);
    });


    console.log("DisplayList");
    console.log(list);
}

function generateTypeStyles(style : any) {
    let tempStyles : any;
    let newTextStyle : any;

    tempStyles = textStyles;
    newTextStyle = {};

    if(tempStyles[style] !== undefined) {
        Object.keys(tempStyles[style]).forEach(e=>{
            [
                'fontFamily',
                'fontSize',
                'fontStyle',
                'backgroundColor',
                'color',
                'stroke',
                'strokeThickness',
                'shadow',
                'padding',
                'align',
                'maxLines',
                'fixedWidth',
                'fixedHeight',
                'resolution',
                'rtl',
                'testString',
                'baselineX',
                'baselineY',
                'wordWrap',
                'metrics',
            ].forEach(el=>{
                if(e === el) {
                    newTextStyle[e] = tempStyles[style][el];
                }
            })
        });
    }
    return newTextStyle;
}


export default {generateDisplayList,list};