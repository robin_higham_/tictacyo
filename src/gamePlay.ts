import StateMachine from "./gameStates/StateMachine";
import enable from "./gameStates/enable";
import init from "./gameStates/init";
import preGame from "./gameStates/preGame";
import endGame from "./gameStates/endGame";
import reset from "./gameStates/reset";

let stateMachine: any;

function start(scene: any): any {
    stateMachine = new StateMachine();
    stateMachine.setStates(
        {
            init: () => {
                init(scene)
            },
            preGame: () =>{
                preGame();
            },
            enable: () => {
                enable()
            },
            endGame: () => {
                endGame();
            },
            reset: () => {
                reset();
            },
        },
    );

    stateMachine.init();
}

function nextState(): any {
    return stateMachine.next();
}

export default {start, nextState}