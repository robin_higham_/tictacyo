export default {
    basic : {
        fontFamily: "Verdana",
        color: '#fff',
        fontSize : 100,
        align:'center',
        strokeThickness : 5,
        stroke: "#000"
    }
}