export default {
    background:{
        type:'sprite',
        texture:"background",
        x:705,
        y:520
    },
    playButton:{
        type:'button',
        x:720,
        y:520,
        states : {
            enabled :"buttonEnabled",
            over: "buttonOver",
            pressed: "buttonPressed",
        }
    },
    replayButton:{
        type:'button',
        x:720,
        y:520,
        states : {
            enabled :"buttonEnabled",
            over: "buttonOver",
            pressed: "buttonPressed",
        }
    },
    playerWinsValue:{
        type:'text',
        string:"0",
        style:"basic",
        x:140,
        y:400,
    },
    computerWinsValue:{
        type:'text',
        string:"0",
        style:"basic",
        x:1220,
        y:400,
    }
}