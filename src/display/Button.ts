import * as Phaser from 'phaser';

export default class Button extends Phaser.GameObjects.Sprite {
    pressState : any;
    overState : any;
    enabledState : any;

    constructor(props : any,textureStates : any) {
        super(props.scene,props.x,props.y,textureStates.enabled);

        this.pressState = textureStates.pressed;
        this.overState = textureStates.over;
        this.enabledState = textureStates.enabled;

        this.on('pointerover',()=>{
            this.setTexture(this.overState);
        });
        this.on('pointerout',()=>{
            this.setTexture(this.enabledState);
        });
        this.on('pointerdown',()=>{
            this.setTexture(this.pressState);
        });
    }

    public enable() {
        this.setInteractive();
    }
    public disabled() {
        this.disableInteractive();
    }
}