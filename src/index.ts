import * as Phaser from 'phaser';
import gameScene from "./gameElements/GameScene";
import gameLogic from "./gameElements/gameLogic";

const gameConfig: Phaser.Types.Core.GameConfig = {
    title: 'Game',
    type: Phaser.AUTO,
    width: 1410,
    height: 1040,
    scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH
    },
    physics: {
        default: 'arcade',
        arcade: {
            debug: true,
        },
    },
    parent: 'game',
    backgroundColor: '#000000',
};

export const game = new Phaser.Game(gameConfig);

game.scene.add('gameScene',gameScene);
game.scene.start('gameScene');
